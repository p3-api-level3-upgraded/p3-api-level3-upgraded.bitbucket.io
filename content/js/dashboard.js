/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 100.0, "KoPercent": 0.0};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.1888780009799118, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [0.625, 500, 1500, "getStaffHealthRecords"], "isController": false}, {"data": [0.0, 500, 1500, "getCentreHolidaysOfYear"], "isController": false}, {"data": [0.0, 500, 1500, "getPastChildrenData"], "isController": false}, {"data": [0.043689320388349516, 500, 1500, "suggestProgram"], "isController": false}, {"data": [0.5154639175257731, 500, 1500, "findAllWithdrawalDraft"], "isController": false}, {"data": [0.09803921568627451, 500, 1500, "findAllLevels"], "isController": false}, {"data": [0.028735632183908046, 500, 1500, "findAllClass"], "isController": false}, {"data": [0.5416666666666666, 500, 1500, "getStaffCheckInOutRecords"], "isController": false}, {"data": [0.17, 500, 1500, "getChildrenToAssignToClass"], "isController": false}, {"data": [0.5858585858585859, 500, 1500, "getCountStaffCheckInOut"], "isController": false}, {"data": [0.0, 500, 1500, "searchBroadcastingScope"], "isController": false}, {"data": [0.46634615384615385, 500, 1500, "getTransferDrafts"], "isController": false}, {"data": [0.0, 500, 1500, "leadByID"], "isController": false}, {"data": [0.0, 500, 1500, "registrationByID"], "isController": false}, {"data": [0.0, 500, 1500, "getRegEnrolmentForm"], "isController": false}, {"data": [0.0, 500, 1500, "findAllLeads"], "isController": false}, {"data": [0.0, 500, 1500, "getEnrollmentPlansByYear"], "isController": false}, {"data": [0.057291666666666664, 500, 1500, "getAvailableVacancy"], "isController": false}, {"data": [0.0, 500, 1500, "getRegistrations"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 2041, 0, 0.0, 40106.804507594265, 3, 331303, 14069.0, 107452.99999999997, 150494.19999999998, 323431.2, 5.802037109158463, 31.4979459854082, 11.628306931067762], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["getStaffHealthRecords", 220, 0, 0.0, 1660.6136363636358, 3, 22272, 196.5, 5450.800000000002, 6613.099999999999, 12455.149999999969, 0.7305426603707172, 0.43019260176127194, 0.9000222016480378], "isController": false}, {"data": ["getCentreHolidaysOfYear", 103, 0, 0.0, 29483.970873786413, 1533, 80460, 24974.0, 52318.0, 57146.6, 79924.27999999991, 0.3276727598953992, 0.8623250241380297, 0.42079070240473626], "isController": false}, {"data": ["getPastChildrenData", 108, 0, 0.0, 100469.36111111111, 34763, 162572, 96318.5, 150512.7, 151584.25, 162239.81, 0.3179303909071908, 0.6449537887808842, 0.7249682253596588], "isController": false}, {"data": ["suggestProgram", 103, 0, 0.0, 12556.873786407768, 319, 53955, 9956.0, 29069.60000000003, 36785.399999999994, 53850.599999999984, 0.3384194824481857, 0.27430485393749426, 0.42004996307777737], "isController": false}, {"data": ["findAllWithdrawalDraft", 97, 0, 0.0, 2182.1752577319585, 26, 30590, 785.0, 5698.4000000000015, 10088.599999999999, 30590.0, 0.3269945826773777, 0.2117162190577162, 1.0675223534086655], "isController": false}, {"data": ["findAllLevels", 102, 0, 0.0, 8240.411764705881, 154, 40906, 5042.0, 19845.5, 30507.099999999995, 40777.35999999999, 0.3409797484772914, 0.2171081992257754, 0.2857037345639805], "isController": false}, {"data": ["findAllClass", 87, 0, 0.0, 12043.367816091952, 397, 44142, 7649.0, 26984.40000000001, 36384.799999999996, 44142.0, 0.2843323093012615, 28.32021994390973, 0.22990932822406693], "isController": false}, {"data": ["getStaffCheckInOutRecords", 96, 0, 0.0, 1818.447916666667, 20, 10855, 800.5, 4842.099999999999, 8420.75, 10855.0, 0.34145595396035555, 0.20240601958392168, 0.446827127252809], "isController": false}, {"data": ["getChildrenToAssignToClass", 100, 0, 0.0, 5952.500000000002, 26, 38730, 3974.0, 12254.6, 20622.849999999948, 38677.67999999997, 0.3253185682079176, 0.18521555201681245, 0.23858812961342393], "isController": false}, {"data": ["getCountStaffCheckInOut", 99, 0, 0.0, 2097.69696969697, 11, 35017, 597.0, 5576.0, 9777.0, 35017.0, 0.330675680640776, 0.19536990897233347, 0.33713419002829115], "isController": false}, {"data": ["searchBroadcastingScope", 104, 0, 0.0, 104213.31730769233, 52007, 158543, 102756.0, 136517.0, 145249.25, 158302.5, 0.295648564825212, 0.5772526013164889, 0.5794596382853521], "isController": false}, {"data": ["getTransferDrafts", 104, 0, 0.0, 2628.01923076923, 9, 20052, 1033.5, 6317.0, 12899.25, 19963.100000000006, 0.3486843869860258, 0.22405695960625485, 0.6050899957755546], "isController": false}, {"data": ["leadByID", 95, 0, 0.0, 38945.43157894738, 14142, 77839, 38358.0, 59440.00000000001, 71949.79999999997, 77839.0, 0.30514637389745797, 0.6962908253727603, 1.3779265946307087], "isController": false}, {"data": ["registrationByID", 88, 0, 0.0, 48613.30681818182, 20020, 98214, 44853.5, 80677.30000000002, 94006.9, 98214.0, 0.2729469273309823, 0.4249590288827476, 1.2415886596755035], "isController": false}, {"data": ["getRegEnrolmentForm", 107, 0, 0.0, 22734.69158878504, 1521, 78110, 23051.0, 40781.200000000004, 44742.6, 76255.84000000004, 0.34754364596021114, 0.6227510911490053, 1.4858848457166058], "isController": false}, {"data": ["findAllLeads", 88, 0, 0.0, 69129.92045454546, 38765, 138444, 65535.5, 100664.6, 112354.89999999995, 138444.0, 0.2576097329640927, 0.4887072071445718, 0.5831439072370771], "isController": false}, {"data": ["getEnrollmentPlansByYear", 147, 0, 0.0, 201168.1156462583, 40012, 331303, 187803.0, 325363.4, 327165.0, 331096.6, 0.4330994007318496, 0.4835626393946626, 0.7185897283627075], "isController": false}, {"data": ["getAvailableVacancy", 96, 0, 0.0, 12510.593749999998, 198, 62114, 8824.0, 31723.1, 38652.45, 62114.0, 0.3131197161047907, 0.17154312571756603, 0.41433321808788226], "isController": false}, {"data": ["getRegistrations", 97, 0, 0.0, 50064.278350515466, 23124, 111043, 45787.0, 79392.8, 94983.99999999999, 111043.0, 0.29660433898512395, 0.6493893524393413, 0.915881757686486], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": []}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 2041, 0, null, null, null, null, null, null, null, null, null, null], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
